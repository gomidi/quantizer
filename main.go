package main

import (
	"fmt"
	"os"

	"gitlab.com/golang-utils/config/v2"
	"gitlab.com/gomidi/quantizer/lib/quantizer"
)

var (
	cfg      = config.New("quantizer", "quantizer is an opinionated quantizer for SMF (MIDI) files", config.AsciiArt("quantizer"))
	argSrc   = cfg.LastString("file", "the MIDI file that is quantized", config.Required())
	argOut   = cfg.String("out", "the file that is written to", config.Required(), config.Shortflag('o'))
	argDelay = cfg.Bool("delay", "detect track delay", config.Shortflag('d'), config.Default(true))
)

func main() {
	err := run()

	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n"+cfg.Usage(), err.Error())
		os.Exit(1)
	}
	os.Exit(0)
}

func run() error {
	err := cfg.Run()

	if err != nil {
		return err
	}

	var opts []quantizer.Option

	if argDelay.Get() {
		opts = append(opts, quantizer.DetectDelay())
	}

	return quantizer.QuantizeFile(argSrc.Get(), argOut.Get(), opts...)
}
