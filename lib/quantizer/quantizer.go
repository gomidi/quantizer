package quantizer

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"os"
	"sort"

	"gitlab.com/gomidi/midi/v2/smf"
)

/*
Idea:

- opinionated smf quantizer
- [x] resolution: 16th notes / 8th tripplets
- [x] detect offset
- [x] detect quantized position by first applying offset and then let it fall to the nearest 16th notes or 8th tripplet slot
- later: detect swing factor (up to 8ths)

*/

func getGridTicks(mt smf.MetricTicks) (t16th int64, ttripplet int64) {
	t16th = int64(mt.Ticks16th())
	ttripplet = int64(math.Round(float64(mt.Ticks4th()) / float64(3)))
	return
}

/*
To get the offset, we find the average distance from the first beat on a bar,
where the distance is less then one 16th.
*/

type timesig struct {
	absPos uint64
	num    uint8
	denom  uint8
}

type timesigs []timesig

func (t timesigs) Len() int {
	return len(t)
}

func (t timesigs) Swap(a, b int) {
	t[a], t[b] = t[b], t[a]
}

func (t timesigs) Less(a, b int) bool {
	return t[a].absPos < t[b].absPos
}

type event struct {
	track   int16
	absPos  uint64
	channel uint8
	message smf.Message
}

type Quantizer struct {
	timesigs     timesigs
	events       []event
	t16th        int64
	ttripplet    int64
	err          error
	barPositions []uint64
	trackEnds    map[int16]uint64
	delays       map[int16][16]int64
	end          uint64
	numTracks    int16
	//	smfreaderOptions []smfreader.Option
	detectTrackDelay bool
}

type Option func(*Quantizer)

func DetectDelay() Option {
	return func(q *Quantizer) {
		q.detectTrackDelay = true
	}
}

/*
func SMFReaderOptions(opts ...smfreader.Option) Option {
	return func(q *Quantizer) {
		q.smfreaderOptions = opts
	}
}
*/

func newQuantizer() *Quantizer {
	return &Quantizer{
		trackEnds: map[int16]uint64{},
		delays:    map[int16][16]int64{},
	}
}

func (o *Quantizer) each(absPos uint64, trackno int16, evt smf.Event) {
	if absPos > o.trackEnds[trackno] {
		o.trackEnds[trackno] = absPos
	}

	if trackno > o.numTracks {
		o.numTracks = trackno
	}

	var channel uint8

	if evt.Message.GetChannel(&channel) {
		var ev event
		ev.track = trackno
		ev.absPos = absPos
		ev.channel = channel
		o.events = append(o.events, ev)
	}

}

func (o *Quantizer) Quantize(track int16, channel uint8, absPos uint64) (newPos int64, isTrippled bool) {
	pos := int64(absPos)
	if o.detectTrackDelay {
		pos = pos - o.GetDelay(track, channel)
	}
	//fmt.Printf("quantize: orig pos: %v, track delay: %v, pos: %v\n", absPos, o.GetDelay(track, channel), pos)
	if pos < o.ttripplet {
		//fmt.Printf("smaller than tripplet: %v\n", pos)
		if pos <= o.t16th {
			//fmt.Printf("smaller than 16th: %v\n", pos)
			if pos > (o.t16th / 2) {
				//fmt.Printf("shift to next 16th: %v\n", o.t16th)
				return o.t16th, false
			} else {
				//fmt.Printf("shift to 0\n")
				return 0, false
			}
		}

		diffTri := o.ttripplet - pos
		diff16th := pos - o.t16th

		if diff16th < diffTri {
			//fmt.Printf("shift to next 16th: %v\n", o.t16th)
			return o.t16th, false
		}

		//fmt.Printf("shift to next tripplet: %v\n", o.ttripplet)
		return o.ttripplet, true
	}

	trimod := pos % o.ttripplet
	t16mod := pos % o.t16th

	//fmt.Printf("tripplet: %v trimod: %v\n", o.ttripplet, trimod)
	//fmt.Printf("16th: %v t16mod: %v\n", o.t16th, t16mod)

	if trimod < t16mod {
		if trimod < (o.ttripplet / 2) {
			//fmt.Printf("shift to previous tripplet: %v\n", pos-trimod)
			return pos - trimod, true
		} else {
			//fmt.Printf("shift to next tripplet: %v\n", pos+o.ttripplet-trimod)
			return pos + o.ttripplet - trimod, true
		}
	}

	if t16mod < (o.t16th / 2) {
		//fmt.Printf("shift to previous 16th: %v\n", pos-t16mod)
		return pos - t16mod, false
	} else {
		//fmt.Printf("shift to next 16th: %v\n", pos+o.t16th-t16mod)
		return pos + o.t16th - t16mod, false
	}
}

func (o *Quantizer) onTimeSig(absPos uint64, num, denom uint8) {
	var ts timesig
	ts.absPos = absPos
	ts.num = num
	ts.denom = denom
	o.timesigs = append(o.timesigs, ts)
}

func (o *Quantizer) onHeader(h *smf.SMF) {
	//fmt.Println("got header")
	if h.Format() == 2 {
		o.err = fmt.Errorf("wrong file format: SMF2")
		return
	}

	mt, ok := h.TimeFormat.(smf.MetricTicks)

	if !ok {
		o.err = fmt.Errorf("wrong timeformat (must be metric ticks")
	}

	o.t16th, o.ttripplet = getGridTicks(mt)
}

func (o *Quantizer) GetDelay(track int16, channel uint8) int64 {
	if channel > 15 {
		return 0
	}

	if track > o.numTracks {
		return 0
	}
	return o.delays[track][int(channel)]
}

func (o *Quantizer) findBarPositionNear(absPos uint64) uint64 {
	var barBefore uint64
	var barnum int

	for i, p := range o.barPositions {
		if p <= absPos {
			barBefore = p
			barnum = i
		}
	}

	if barnum >= len(o.barPositions)-1 {
		return barBefore
	}

	barAfter := o.barPositions[barnum+1]

	if (barAfter - absPos) < (absPos - barBefore) {
		return barAfter
	}

	return barBefore
}

func makeAverageOffset(coffsets []int64) int64 {
	var sum int64

	for _, o := range coffsets {
		sum += o
	}

	r := math.Round(float64(sum) / float64(len(coffsets)))
	return int64(r)
}

// findOffsets finds the offset for each track
func (o *Quantizer) findOffsets() {
	//fmt.Println("findOffsets called")
	o.findBarPositions()

	// map[int16]offset

	registry := map[int16][16][]int64{}

	for _, ev := range o.events {
		b := o.findBarPositionNear(ev.absPos)

		diff := int64(ev.absPos) - int64(b)
		if math.Abs(float64(diff)) < float64(o.t16th) {
			toffs := registry[ev.track]
			toffs[ev.channel] = append(toffs[ev.channel], diff)
			registry[ev.track] = toffs
		}
	}

	for tr, mix := range registry {
		for ch, coffsets := range mix {
			toffs := o.delays[tr]
			toffs[ch] = makeAverageOffset(coffsets)
			o.delays[tr] = toffs
		}
	}

	return
}

func (o *Quantizer) findTimeSigAt(pos uint64) (num, denom uint8) {
	num = uint8(4)
	denom = uint8(4)

	for _, ts := range o.timesigs {
		if ts.absPos <= pos {
			num = ts.num
			denom = ts.denom
		}
	}
	return
}

func (o *Quantizer) setEnd() {
	var end uint64

	for _, e := range o.trackEnds {
		if e > end {
			end = e
		}
	}

	o.end = end

}

func (o *Quantizer) findBarPositions() {
	sort.Sort(o.timesigs)
	o.setEnd()

	var end uint64

	for {
		num, denom := o.findTimeSigAt(end)

		if num+denom == 0 {
			return
		}
		end += o.barLength(num, denom)
		if end > o.end {
			return
		}
		o.barPositions = append(o.barPositions, end)
	}
}

func (o *Quantizer) barLength(num, denom uint8) uint64 {
	a := float64(o.t16th) * 16.0 * float64(num)
	res := uint64(math.Round(a / float64(denom)))
	if res == 0 {
		panic("bar length can't be 0")
	}
	return res
}

func New(src io.Reader, opts ...Option) (*Quantizer, error) {
	var o = newQuantizer()

	for _, opt := range opts {
		opt(o)
	}

	rd, err := smf.ReadFrom(src)

	if err != nil {
		//panic(err.Error())
		return nil, err
	}

	o.onHeader(rd)

	var abspos uint64

	for trackno, tr := range rd.Tracks {
		abspos = 0
		for _, ev := range tr {
			abspos += uint64(ev.Delta)
			o.each(abspos, int16(trackno), ev)
		}
	}

	/*
		rd := reader.New(
			reader.NoLogger(),
			reader.SMFHeader(o.onHeader),
			reader.Each(o.each),
			reader.TimeSig(o.onTimeSig),
		)
	*/

	/*
		err := reader.ReadSMF(rd, src, o.smfreaderOptions...)

		if err != nil {
			return nil, err
		}

		if o.err != nil {
			return nil, o.err
		}
	*/

	o.findOffsets()

	return o, nil
}

func Quantize(rd io.Reader, wr io.Writer, opts ...Option) error {
	var sm = NewSMFQuantizer()

	bt, err := ioutil.ReadAll(rd)

	if err != nil {
		return err
	}

	in := bytes.NewReader(bt)

	sm.quantizer, err = New(in, opts...)

	if err != nil {
		return err
	}

	//fmt.Println(len(bt))
	in = bytes.NewReader(bt)

	r, err := smf.ReadFrom(in)
	if err != nil {
		//panic(err.Error())
		return err
	}

	sm.onHeader(r)

	/*
			r := reader.New(
				reader.NoLogger(),
				reader.SMFHeader(sm.onHeader),
				reader.Each(sm.each),
			)

			err = reader.ReadSMF(r, in)

			if err != nil {
			return err
		}
	*/
	var absPos uint64

	for trackno, tr := range r.Tracks {
		absPos = 0
		for _, evt := range tr {
			absPos += uint64(evt.Delta)
			sm.each(absPos, int16(trackno), evt)
		}
	}

	return sm.WriteTo(wr)

}

func QuantizeFile(srcfile, targetfile string, opts ...Option) error {
	i, err := os.Open(srcfile)

	if err != nil {
		return err
	}

	defer i.Close()

	o, err := os.Create(targetfile)

	if err != nil {
		return err
	}

	defer o.Close()

	return Quantize(i, o, opts...)
}

type events []event

func (e events) Len() int {
	return len(e)
}

func (e events) Less(a, b int) bool {
	return e[a].absPos < e[b].absPos
}

func (e events) Swap(a, b int) {
	e[a], e[b] = e[b], e[a]
}

func NewSMFQuantizer() *smfQuantizer {
	return &smfQuantizer{
		tracks: map[int16]events{},
	}
}

type smfQuantizer struct {
	quantizer *Quantizer
	header    *smf.SMF
	tracks    map[int16]events
	err       error
}

func (o *smfQuantizer) WriteTo(w io.Writer) error {
	var sm *smf.SMF

	switch o.header.Format() {
	case 0:
		sm = smf.New()
	case 1:
		sm = smf.NewSMF1()
	case 2:
		sm = smf.NewSMF2()
	}

	sm.TimeFormat = o.header.TimeFormat

	//wr := writer.NewSMF(w, o.header.NumTracks, opts...)
	//numTracks := int16(o.header.NumTracks)
	numTracks := int16(len(o.header.Tracks))

	for t := int16(0); t < numTracks; t++ {
		var tr smf.Track
		evts := o.tracks[t]

		//	fmt.Printf("in track %v\n", t)

		sort.Sort(evts)

		var lastTick uint64

		for _, ev := range evts {
			tr.Add(uint32(ev.absPos-lastTick), ev.message)
			//fmt.Printf("abspos: %v\nset delta: %v\n", ev.absPos, uint32(ev.absPos-lastTick))
			//wr.SetDelta(uint32(ev.absPos - lastTick))
			//wr.Write(ev.message)
			//fmt.Printf("write message: %s\n", ev.message.String())
			lastTick = ev.absPos
		}

		//wr.Write(meta.EndOfTrack)
		tr.Close(0)
		sm.Add(tr)
	}

	sm.WriteTo(w)

	return nil
}

func (o *smfQuantizer) onHeader(h *smf.SMF) {
	o.header = h
}

func (o *smfQuantizer) each(absPos uint64, trackno int16, evt smf.Event) {
	if evt.Message.Is(smf.MetaEndOfTrackMsg) {
		return
	}
	var ev event
	ev.message = evt.Message
	ev.track = trackno
	ev.absPos = absPos

	var channel uint8

	if evt.Message.GetChannel(&channel) {
		_absPos, _ := o.quantizer.Quantize(trackno, channel, absPos)
		ev.channel = channel
		ev.absPos = uint64(_absPos)
	}

	/*
		if chmsg, ok := msg.(channel.Message); ok {
			absPos, _ := o.quantizer.Quantize(pos.Track, chmsg.Channel(), pos.AbsoluteTicks)
			ev.channel = chmsg.Channel()
			ev.absPos = uint64(absPos)
		}
	*/

	//fmt.Printf("event: @%v %s\n", ev.absPos, ev.message.String())

	o.tracks[trackno] = append(o.tracks[trackno], ev)
}
