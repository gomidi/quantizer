package quantizer

import (
	"bytes"
	"fmt"
	"strings"
	"testing"

	"gitlab.com/gomidi/midi/v2"
	"gitlab.com/gomidi/midi/v2/smf"
)

func mkSMF(offset1, offset2 uint32) []byte {
	var bf bytes.Buffer

	//wr := writer.NewSMF(&bf, 2, smfwriter.TimeFormat(smf.MetricTicks(960)))

	wr := smf.NewSMF1()
	var tr1 smf.Track
	var tr2 smf.Track

	tr1.Add(0, smf.MetaMeter(4, 4))
	tr1.Add(offset1, smf.Message(midi.NoteOn(2, 120, 100)))
	tr1.Add(930, smf.Message(midi.NoteOff(2, 120)))
	tr1.Add(0, smf.Message(midi.NoteOn(2, 121, 100)))
	tr1.Add(990, smf.Message(midi.NoteOff(2, 121)))
	tr1.Add(0, smf.Message(midi.NoteOn(2, 122, 100)))
	tr1.Add(960, smf.Message(midi.NoteOff(2, 122)))
	tr1.Add(0, smf.Message(midi.NoteOn(2, 123, 100)))
	tr1.Add(960, smf.Message(midi.NoteOff(2, 123)))
	tr1.Add(0, smf.Message(midi.NoteOn(2, 124, 100)))
	tr1.Add(970, smf.Message(midi.NoteOff(2, 124)))
	tr1.Close(0)

	wr.Add(tr1)

	//writer.Meter(wr, 4, 4)

	/*
		wr.SetChannel(2)
		wr.SetDelta(offset1)
		writer.NoteOn(wr, 120, 100)
		wr.SetDelta(930)
		writer.NoteOff(wr, 120)
		writer.NoteOn(wr, 121, 100)
		wr.SetDelta(990)
		writer.NoteOff(wr, 121)
		writer.NoteOn(wr, 122, 100)
		wr.SetDelta(960)
		writer.NoteOff(wr, 122)
		writer.NoteOn(wr, 123, 100)
		wr.SetDelta(960)
		writer.NoteOff(wr, 123)
		writer.NoteOn(wr, 124, 100)
		wr.SetDelta(970)
		writer.NoteOff(wr, 124)
		writer.EndOfTrack(wr)
	*/

	tr2.Add(offset2, smf.Message(midi.NoteOn(4, 20, 100)))
	tr2.Add(860, smf.Message(midi.NoteOff(4, 20)))
	tr2.Add(0, smf.Message(midi.NoteOn(4, 21, 100)))
	tr2.Add(1060, smf.Message(midi.NoteOff(4, 21)))
	tr2.Add(0, smf.Message(midi.NoteOn(4, 22, 100)))
	tr2.Add(960, smf.Message(midi.NoteOff(4, 22)))
	tr2.Add(0, smf.Message(midi.NoteOn(4, 23, 100)))
	tr2.Add(960, smf.Message(midi.NoteOff(4, 23)))
	tr2.Add(0, smf.Message(midi.NoteOn(4, 24, 100)))
	tr2.Add(980, smf.Message(midi.NoteOff(4, 24)))
	tr2.Close(0)

	wr.Add(tr2)
	/*
		wr.SetChannel(4)
		wr.SetDelta(offset2)
		writer.NoteOn(wr, 20, 100)
		wr.SetDelta(860)
		writer.NoteOff(wr, 20)
		writer.NoteOn(wr, 21, 100)
		wr.SetDelta(1060)
		writer.NoteOff(wr, 21)
		writer.NoteOn(wr, 22, 100)
		wr.SetDelta(960)
		writer.NoteOff(wr, 22)
		writer.NoteOn(wr, 23, 100)
		wr.SetDelta(960)
		writer.NoteOff(wr, 23)
		writer.NoteOn(wr, 24, 100)
		wr.SetDelta(980)
		writer.NoteOff(wr, 24)
		writer.EndOfTrack(wr)
	*/

	wr.WriteTo(&bf)

	return bf.Bytes()
}

func TestFindDelays(t *testing.T) {
	offset1 := uint32(10)
	offset2 := uint32(20)

	bt := mkSMF(offset1, offset2)
	q, err := New(bytes.NewReader(bt))

	if err != nil {
		t.Errorf("Error: %s", err.Error())
		return
	}
	d1 := q.GetDelay(0, 2)

	if uint32(d1) != offset1 {
		t.Errorf("delay of track 1 and channel 3 must be %v, but is %v", offset1, d1)
	}

	d2 := q.GetDelay(1, 4)

	if uint32(d2) != offset2 {
		t.Errorf("delay of track 2 and channel 5 must be %v, but is %v", offset2, d2)
	}

}

func readSMF(in []byte) (string, error) {
	var bf bytes.Buffer
	rd, err := smf.ReadFrom(bytes.NewReader(in))

	if err != nil {
		return "", err
	}

	var absPos uint64

	for trackno, tr := range rd.Tracks {
		absPos = 0
		for _, ev := range tr {
			absPos += uint64(ev.Delta)
			bf.WriteString(fmt.Sprintf("[%v] @%v: %s\n", trackno, absPos, ev.Message.String()))
		}

	}

	/*
		rd := reader.New(
			reader.NoLogger(),
			reader.Each(func(pos *reader.Position, msg midi.Message) {
				bf.WriteString(fmt.Sprintf("[%v] @%v: %s\n", pos.Track, pos.AbsoluteTicks, msg.String()))
			}),
		)

		err := reader.ReadSMF(rd, bytes.NewReader(in))
	*/
	return bf.String(), err
}

// TODO test tripplets
func TestQuantize(t *testing.T) {
	offset1 := uint32(10)
	offset2 := uint32(20)

	bt := mkSMF(offset1, offset2)
	//before, _ := readSMF(bt)
	//fmt.Println(before)

	var out bytes.Buffer
	err := Quantize(bytes.NewReader(bt), &out)

	if err != nil {
		t.Errorf("Error: %s", err.Error())
		return
	}

	got, err := readSMF(out.Bytes())

	if err != nil {
		t.Errorf("Error: %s", err.Error())
		return
	}

	got = strings.TrimSpace(got)

	expected := strings.TrimSpace(`
[0] @0: MetaTimeSig meter: 4/4
[0] @0: NoteOn channel: 2 key: 120 velocity: 100
[0] @960: NoteOff channel: 2 key: 120
[0] @960: NoteOn channel: 2 key: 121 velocity: 100
[0] @1920: NoteOff channel: 2 key: 121
[0] @1920: NoteOn channel: 2 key: 122 velocity: 100
[0] @2880: NoteOff channel: 2 key: 122
[0] @2880: NoteOn channel: 2 key: 123 velocity: 100
[0] @3840: NoteOff channel: 2 key: 123
[0] @3840: NoteOn channel: 2 key: 124 velocity: 100
[0] @4800: NoteOff channel: 2 key: 124
[0] @4800: MetaEndOfTrack
[1] @0: NoteOn channel: 4 key: 20 velocity: 100
[1] @960: NoteOff channel: 4 key: 20
[1] @960: NoteOn channel: 4 key: 21 velocity: 100
[1] @1920: NoteOff channel: 4 key: 21
[1] @1920: NoteOn channel: 4 key: 22 velocity: 100
[1] @2880: NoteOff channel: 4 key: 22
[1] @2880: NoteOn channel: 4 key: 23 velocity: 100
[1] @3840: NoteOff channel: 4 key: 23
[1] @3840: NoteOn channel: 4 key: 24 velocity: 100
[1] @4800: NoteOff channel: 4 key: 24
[1] @4800: MetaEndOfTrack
`)

	if got != expected {
		t.Errorf("got: \n%s \n// expected\n%s\n", got, expected)
	}
}
